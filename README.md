# GainAdminPanel
An example of Restful API developed with express.js, firebase and docker.

## How to install and run
---

```bash
$ git clone https://gitlab.com/sDem/gainadminpanel.git
$ cd gainadminpanel
$ npm install
$ npm start
```

## Endpoints and Outputs

### POST /admins/signup

create a new admin user. I used firebase-admin for authentication and save information in Firestore at the same time. 

**Request Body**

| Field    | Type   | Description                       |
| -------- | ------ | --------------------------------- |
| name     | string | user identifier                   |
| email    | string | unique credential                 |
| password | string | for sign in                       |
| birthday | date   | contains date *YYYY-MM-DD* format |

 ### DELETE  /admins/{adminId}

delete a admin user from firebase auth and firestore.  This end point is secure with Authentication header.

**Response Body**

```
{
	"uid" : "user_uid"
}
```

**example Request**

```
curl --request DELETE 'http://localhost:56002/admins/agptT8KskMWJwvZRvkTbNUO9JOH3' \
--header 'Authorization: Bearer TOKEN' \
--header 'Content-Type: application/json' \
```

### GET  /contents/movies/{contentId}

get movie details with content id credential.

**Response Body**

```
{
	"content-type" : "movie"
	"name" : "kahpe bizans",
	"description" : "historical facts about Byzantium empire",
}
```

**example Request**

```
curl --request GET 'http://localhost:56002/contents/movies/agptT8KskMWJwvZRvkTbNUO9JOH3' \
--header 'Authorization: Bearer TOKEN' \
--header 'Content-Type: application/json' \
```

### POST /contents/movies

add a new movies to Firestore. This end-point is secure with Authentication header.

**Request Body**

| Field       | Type   | Description              |
| ----------- | ------ | ------------------------ |
| name        | string | name of the movie        |
| description | string | description of the movie |

**Response Body**

```
{
	idString
}
```

**example Request**

```
curl --request POST 'http://localhost:56002/contents/movies' \
--header 'Authorization: Bearer agptT8KskMWJwvZRvkTbNUO9JOH3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name" : "kill bill",
    "description" : "an angry bride trying to catch her groom"
}'
```

### POST /contents/{contentId}/media

add a new movies media to specific movie. This end point is secure with Authentication header.

**Request Body**

| Field | Type | Description   |
| ----- | ---- | ------------- |
| file  | file | file of media |

### GET /contents/series/{contentId}

get series details with contentId. This end point is secure with Authentication header.

**Response Body**

```
{
	"content-type" : "series"
	"name" : "ezel",
	"description" : "a man who loses everything return for revenge but not in Marseille ",
	"tags" : [thirteen_and_up]
}
```

**example Request**

```
curl --request GET 'http://localhost:56002/contents/series/7saqg3NIdFHcEIJZG1Co' \
--header 'Authorization: Bearer agptT8KskMWJwvZRvkTbNUO9JOH3'
```

### POST /contents/series

add a new series to Firestore. This end point is secure with Authentication header.

**Request Body**

| Field       | Type   | Description               |
| ----------- | ------ | ------------------------- |
| name        | string | name of the series        |
| description | string | description of the series |

**Response Body**

```
{
	idString
}
```

### POST /contents/series/{contentId}/season

add a new season to existing series. This end point is secure with Authentication header.

**Request Body**

| Field       | Type   | Description                             |
| ----------- | ------ | --------------------------------------- |
| name        | string | name of the series                      |
| description | string | description of the series               |
| episodes    | array  | its includes episodes, id and media url |

**Response Body**

```
{
	idString
}
```

### POST /contents/series/{contentId}/season/{seasonId}/episode/{episodeId}/media

add a new episode media to storage. This end point is secure with Authentication header.

TODO

---

**TODOS**

* Add test
* Add media post endpoints
* Add sing in 
