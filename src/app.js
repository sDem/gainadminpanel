const express = require('express');
const helmet = require('helmet');
const cors = require('cors');

const app = express();

const config = require('./config/defaut');
const { verifyToken } = require('./middlewares/verifyToken');
const { handleLog } = require('./middlewares/logHandler');
const { handleError } = require('./middlewares/errorHandler');

const admin = require('./routes/admin.routes.js');
const content = require('./routes/content.routes.js');

app.use(express.urlencoded({extended : true}));
app.use(express.json());
app.use(helmet());
app.use(cors());
app.use(function(req, res, next) {
    verifyToken(req, res, next);
});

app.use(function(req, res, next){
    handleLog(req, res, next);
});

app.use('/admins', admin);
app.use('/contents', content);

app.use(function(err, req, res, next) {
    handleError(err, res);
});

app.use(function(req, res, next){
    res.status(400).send({code : "400", message : "Request not found!"});
});

const PORT = config.PORT || 56002;
module.exports = app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});
