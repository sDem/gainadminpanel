const admin = require('firebase-admin');
const { getFirestore } = require('firebase-admin/firestore');
const serviceAccountConfig = require('../credentials/gain-admin-panel-firebase-adminsdk-bxw5a-1fc0f7be62.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccountConfig),
    storageBucket: "gs://gain-admin-panel.appspot.com"

});

const db = getFirestore();

const storage = admin.storage().bucket("movies");

module.exports = {
    admin,
    db, 
    storage
};