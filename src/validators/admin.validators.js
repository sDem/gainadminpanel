const Joi = require('joi');

const validateAdminRegister = (adminCredentials) => {
    const schema = Joi.object({
        name : Joi
            .string()
            .required(),
        email : Joi
            .string()
            .required(),
        password : Joi
            .string()
            .required(),
        birthday : Joi
            .date()
            .required()
    });

    return schema.validate(adminCredentials);
}

module.exports = {
    validateAdminRegister
}