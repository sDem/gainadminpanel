const Joi = require('joi');

const validateMovie = (movieCredentials) => {
    const schema = new Joi.object({
        name : Joi
            .string()
            .required(),
        description : Joi
            .string()
            .required()
    });

    return schema.validate(movieCredentials);
}

const validateSeries = (seriesCredential) => {
    const schema = new Joi.object({
        name : Joi
            .string()
            .required(),
        description : Joi
            .string()
            .required(),
        tags : Joi
            .array()
            .items(Joi.string().valid("for_all", "seven_and_up", "thirteen_and_up", "eighteen_and_up", "violence", "sexuality","bad_behaviors"))        
    });

    return schema.validate(seriesCredential);
}

const validateSeason = (seasonCredentials) => {
    const schema = Joi.object({
        name : Joi
            .string()
            .required(),
        description : Joi
            .string()
            .required(),
        episodes : Joi
            .array()
            .required()
    });

    return schema.validate(seasonCredentials);
}

module.exports = { 
    validateMovie,
    validateSeries,
    validateSeason
}