const express = require('express');
const router = express.Router();
const { ErrorHandler } = require('../middlewares/errorHandler');
const adminValidator = require('../validators/admin.validators');
const adminService = require('../controller/admin.controller.js');

//create an user
router.post('/signup', (req, res, next) => {
    const { error } = adminValidator.validateAdminRegister(req.body);
    if(error) throw new ErrorHandler(400, error.message);

    adminService.adminSignup(req.body)
        .then((data) => {
            console.log(data);
            res.status(200).json(data);
        })
        .catch((registerError) => {
            next(registerError);
        });
});

//delete an user
router.delete('/:id', (req, res, next) => {
    const uid = req.params.id;
    if(!uid) throw new ErrorHandler(400, 'uid is required');

    adminService.deleteUser(uid)
        .then(() => {
            res.status(200).json({uid : uid});
        })  
        .catch((error) => {
            next(error);
        });
});

module.exports = router;