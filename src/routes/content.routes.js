const express = require('express');
const multer = require('multer');
const { ErrorHandler } = require('../middlewares/errorHandler');
const { validateMovie, validateSeries, validateSeason } = require('../validators/content.validator');
const contentController = require('../controller/content.controller');

const router = express.Router();

//TODO : also has to validate file type.
const upload = multer({
    storage: multer.memoryStorage()
});

//get movie detail with movie id
router.get('/movies/:movieId', (req, res, next) => {
    const movieId = req.params.movieId;
    if (!movieId) throw new ErrorHandler(400, 'movie id is required');

    contentController.getMovie(movieId)
        .then((data) => {
            res.status(200).json(data)
        })
        .catch((error) => {
            next(error);
        });
});

//add new movie to contents collection
router.post('/movies', (req, res, next) => {
    const { error } = validateMovie(req.body);
    if (error) throw new ErrorHandler(400, error.message);

    const movieCredentials = req.body;

    contentController.createMovie(movieCredentials)
        .then((data) => {
            res.status(200).json(data);
        })
        .catch((movieError) => {
            next(movieError);
        })
});

//add movie media to movie
router.post('/movies/:movieId/media', upload.single('file'), (req, res, next) => {
    const movieId = req.params.movieId;
    if (!movieId) throw new ErrorHandler(400, 'movieId not found');

    if (!req.file) throw new ErrorHandler(400, 'file not found');

    contentController.addMovieMedia(movieId, req.file)
        .then((data) => {
            res.status(200).json(data)
        })
        .catch((error) => {
            next(error);
        })
});

//get series details with series id
router.get('/series/:seriesId', (req, res, next) => {
    const seriesId = req.params.seriesId;
    if (seriesId) throw new ErrorHandler(400, "series id not found");

    contentController.getSeries(seriesId)
        .then((data) => {
            res.status(200).json(data);
        })
        .catch((error) => {
            next(error);
        })
});

//add new series
router.post('/series', (req, res, next) => {
    const { error } = validateSeries(req.body);
    if (error) throw new ErrorHandler(400, error.message);

    const seriesCredentials = req.body;

    contentController.createSeries(seriesCredentials)
        .then((data) => {
            res.status(200).json(data);
        })
        .catch((seriesError) => {
            next(seriesError);
        });
});

//add new session to a series
router.post('/series/:seriesId/season', (req, res, next) => {
    const seriesId = req.params.seriesId;
    if (!seriesId) throw new ErrorHandler(400, 'series id not found');

    const { error } = validateSeason(req.body);
    if (error) throw new ErrorHandler(400, error.message);

    contentController.createSeason(seriesId, req.body)
        .then((data) => {
            res.status(200).json(data);
        })
        .catch((seasonError) => {
            next(seasonError);
        })
});

//add new episode media to a episode of series.
router.post('/series/:seriesId/season/:seasonId/episode/:episodeId/media', upload.single("file"), (req, res, next) => {
    const { seriesId, seasonId, episodeId } = req.params;
    if (!seriesId || !seasonId || !episodeId) throw new ErrorHandler(400, 'missing id parameter');

    if (!req.file) throw new ErrorHandler(400, 'file not found');

    contentController.addEpisodeMedia(seriesId, seasonId, episodeId, req.file)
        .then((data) => {
            res.status(200).json(data)
        })
        .catch((error) => {
            next(error);
        })
});

module.exports = router;