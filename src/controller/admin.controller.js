/* eslint-disable no-async-promise-executor */
const { ErrorHandler } = require('../middlewares/errorHandler');
const { admin, db } = require('../services/firebase');


/**
 * create a user and return user credentials
 * 
 * @param {Object} adminCredentials : admin user informations
 */
async function adminSignup(adminCredentials) {
    return new Promise((resolve, reject) => {
        const { email, password, name, birthday } = adminCredentials;

        admin.auth().createUser({
            email: email,
            password: password,
            displayName: name,
            emailVerified: false,
            disabled: false
        })
            .then(async (data) => {
                try {
                    const doc = await db.collection('users').doc(data.uid);
                    const newUser = Object.assign({ email: data.email, name: data.displayName, password: password, uid: data.uid }, { birthday: birthday });
                    await doc.set(newUser);
                    resolve(data);
                } catch (error) {
                    reject(new ErrorHandler(500, error.message));
                }

            })
            .catch((error) => {
                reject(new ErrorHandler(500, error.message));
            });
    });
}

/**
 * delete an user with uid
 * 
 * @param {string} uid : unique identifier of user
 * @returns {uid} : 
 */
const deleteUser = (uid) => {
    return new Promise(async (resolve, reject) => {
        admin.auth().deleteUser(uid)
            .then(async () => {
                const doc = await db.collection('users').doc(uid);
                doc.delete();
                resolve(uid)
            })
            .catch((error) => {
                reject(new ErrorHandler(500, error.message));
            });
    });
}

module.exports = {
    adminSignup,
    deleteUser
}