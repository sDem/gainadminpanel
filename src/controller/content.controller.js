const { db, storage, admin } = require('../services/firebase');
const { ErrorHandler } = require('../middlewares/errorHandler');

/**
 * get movie details with movie id
 * 
 * @param {string} id : unique movie id 
 * @returns {Object} : movie detail object
 */
const getMovie = async (id) => {
    try {
        const contentRef = await db.collection('contents').doc(id);
        const movie = await contentRef.get();

        if (!movie) throw new (404, 'content not found');

        return movie._fieldsProto;
    } catch (error) {
        let statusCode = error.statusCode || 500
        throw new ErrorHandler(statusCode, error.message);
    }
}

/**
 * create new movie without content media.
 * seperating this services, routes and collections are better
 * 
 * @param {Object} movieCredentials : movie details
 * @returns {string} : movie id
 */
const createMovie = async (movieCredentials) => {
    try {
        movieCredentials = Object.assign(movieCredentials, { "content_type": "movie" });
        const contentRef = await db.collection('contents').add(movieCredentials);

        return contentRef.id;
    } catch (error) {
        throw new ErrorHandler(500, error.message);
    }
}

/**
 * add media to movie
 * 
 * @param {string} movieId : unique movie id 
 * @param {*} movieMedia :  file
 */
const addMovieMedia = async (movieId, movieMedia) => {
    try {
        const blob = storage.file(movieMedia.originalname);

        const blobWriter = blob.createReadStream({
            metadata: {
                contentType: movieMedia.mimetype
            }
        });

        blobWriter.on('error', (err) => {
            console.log(err);
        });

        blobWriter.on('finish', () => {
            return 'success'
        });

        blobWriter.end(movieMedia.buffer);
    } catch (error) {
        throw new ErrorHandler(500, error.message);
    }
}

/**
 * create new series and returns new id.
 * 
 * @param {Object} seriesCredentials : series detail credentials
 * @returns {string} : series id
 */
const createSeries = async (seriesCredentials) => {
    try {
        seriesCredentials = Object.assign(seriesCredentials, { seasons: [] });
        seriesCredentials = Object.assign(seriesCredentials, { "content_type": "series" });
        const contentRef = await db.collection('contents').add(seriesCredentials);

        return contentRef.id;
    } catch (error) {
        throw new ErrorHandler(500, error.message);
    }
}

/**
 * get series details with series id
 * 
 * @param {string} id : unique series id 
 * @returns {Object} : series detail object
 */
const getSeries = async (id) => {
    try {
        const contentRef = await db.collection('contents').doc(id);
        const series = await contentRef.get();

        if (!series) throw new ErrorHandler(404, "content not found");

        return series.id;
    } catch (error) {
        const statusCode = error.statusCode || 500;
        throw new ErrorHandler(statusCode, error.message);
    }
}

/**
 * create a new season
 * 
 * @param {string} seriesId : unique id for series
 * @param {Object} seasonCredentials : season details
 * @returns {string} : new seasson id 
 */
const createSeason = async (seriesId, seasonCredentials) => {
    try {
        const contentRef = await db.collection('contents').doc(seriesId);

        const res = await contentRef.update({
            seasons: admin.firestore.FieldValue.arrayUnion(seasonCredentials)
        });

        return res.id;
    } catch (error) {
        throw new ErrorHandler(500, error.message);
    }
}

/**
 * add media to episode
 * 
 * @param {*} seriesId : unique id for series
 * @param {*} seasonId : unique id for season
 * @param {*} episodeId : unique id for episode
 * @param {*} episodeMedia : file
 */
const addEpisodeMedia = async (seriesId, seasonId, episodeId, episodeMedia) => {
    try {
        const blob = storage.file(episodeMedia.originalname);

        const blobWriter = blob.createReadStream({
            metadata: {
                contentType: episodeMedia.mimetype
            }
        });

        blobWriter.on('error', (err) => {
            console.log(err);
        });

        blobWriter.on('finish', () => {
            return 'success'
        });

        blobWriter.end(episodeMedia.buffer);
    } catch (error) {
        throw new ErrorHandler(500, error.message);
    }
}

module.exports = {
    createMovie,
    getMovie,
    createSeries,
    getSeries,
    createSeason,
    addMovieMedia,
    addEpisodeMedia
}