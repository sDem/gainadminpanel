const dotenv = require('dotenv');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

dotenv.config();

module.exports = {
    
    PORT : parseInt(process.env.PORT, 10) || 56002
}
