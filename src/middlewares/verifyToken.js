const admin = require('firebase-admin');
const { ErrorHandler } = require('./errorHandler');

const verifyToken = async (req, res, next) => {
    const nonSecurePaths = ['/', '/admins/signup']
    if (nonSecurePaths.includes(req.path)) return next();

    const authHeader = req.headers.authorization;

    if (authHeader) {
        const idToken = authHeader.split(" ")[1];
        admin
            .auth()
            .verifyIdToken(idToken)
            .then(function (decodedToken) {
                return next();
            })
            .catch(function (error) {
                next(new ErrorHandler(401, error.message));
            });
    } else {
        next(new ErrorHandler(401, 'invalid token'));
    }
}

module.exports = { verifyToken };