class ErrorHandler extends Error {
    constructor(statusCode, errorMessage){
        super();
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
    }
}

const handleError = (err, res) => {
    const { statusCode, errorMessage } = err;
    res.status(statusCode).json({
        code : statusCode,
        message : errorMessage
    });
}

module.exports = {
    ErrorHandler,
    handleError
}