module.exports = {
    handleLog : (req, res, next) => {
        let time = new Date().toLocaleDateString();
        console.log(`${time} - [${req.method}] - ${req.path}`);
        next();
    }
}