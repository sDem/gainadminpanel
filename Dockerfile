FROM node:16

# creating an app directory
WORKDIR /src

COPY package*.json ./

RUN npm install

COPY ./src .

EXPOSE 56002

CMD ["node", "app.js"]
